#include <iostream>
#include "E101.h"

class Robot { //decalarations
	private: //declearing private fields (only this class can change)
		int v_left, v_right, cam_tilt;
		int dv;
		double line_error;
		int quadrant;
		const int cam_width = 320;
		const int cam_height = 240;
		const int v_left_go = 52; //This will need to be change
		const int v_right_go = 43; //This will need to be change
		double kp = 0.05;
		int line_present = 1;
	public: //declaring public fields (that other Classes can change)
		Robot(){} //default constructor (labeled same as class)
		//declaring methods
		int initHardware(); 
		void ReadSetMotors(); 
		void SetMotors();
		int MeasureLine();
		int FollowLine();
};
//to create new methods for the robot make sure to put them under the publically
//decleared fields. 
//method set up:
// Class::method(){} //with return value


Robot::initHardware() {
		
		return 0;
}

Robot::ReadSetMotors() {
	

}

Robot::SetMotors(){
	
}

Robot::MeasureLine(){
	
	return 0;
}

Robot::FollowLine(){
	MeasureLine();
	if (line_present) {
		dv = (int)(line_error*kp);
		//dv = 0;
		v_left = v_left_go + dv;
		v_right = v_right_go +dv;
		cout<<" line_error = "<<line_error<<"\tdv = "<<dv<<endl;
		SetMotors(); 
	} else {
		//go back
		cout<<" Line Missing"<<endl;
		v_left = 39;
		v_right = 55;
		SetMotors();
		sleep1(100,1);
	}
	return 0;
}

int main() {
	Robot robot;
	robot.initHardware();
	int count = 0;
	open_screen_stream();
	
	while (count < 5000){
		robot.MeasureLine();
		robot.FollowLine();
		count++;
	}
	close_screen_stream();
	stop(100);
	return 0;
	
}
