#include <iostream>
#include "E101.h"
using namespace std;

int main() {
	//make sure to be connected to wifi
	
	char server_addr[15] = {'1','3','0','.','1','9','5','.','1','9','6'};//on the green sheet of paper by gate
	char openPlease[24] ={'P','l','e','a','s','e'};
	char password[24];// will be set when connected to server
	
	connect_to_server(server_addr,1024)//connects to server with IP address and port 1024
	
	send_to_server(openPlease); //sends connection plea to server
	
	receive_from_server(password);//sever will change 'password' to the password we need
	
	send_to_server(password); //send server the password we received
	
	return 0;

}
