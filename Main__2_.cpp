#include <iostream>
#include "E101.h"

using namespace std;

class Robot { //decalarations
	private: //declearing private fields (only this class can change)
		int v_left, v_right, cam_tilt;
		int dv;
		double line_error;
		int quadrant;
		const int cam_width = 320;
		const int cam_height = 240;
		const int v_left_go = 52; //This will need to be change
		const int v_right_go = 43; //This will need to be change
		double kp = 0.01;
		int line_present = 1;
		int error[320]; 
	public: //declaring public fields (that other Classes can change)
		Robot(){} //default constructor (labeled same as class)
		//declaring methods
		int initHardware(); 
		void ReadSetMotors(); 
		void SetMotors(int vLeft, int vRight);
		int MeasureLine();
		int Quad3();
		int Quad4();
		int FollowLine();
		int OpenGate();
		
};
//to create new methods for the robot make sure to put them under the publically
//decleared fields. 
//method set up:
// Class::method(){} //with return value


int Robot::initHardware() {
		
		int err;
  cout<<" Hello"<<endl;
  err = init(0);
  cout<<"After init() error="<<err<<endl;
  
  //int count = 0;
  take_picture();
  update_screen();
  
	 set_motors(3, 48);
	 set_motors(1, 48);
	 set_motors(5, 48);
	 hardware_exchange();
	 sleep1(1000);
	 set_motors(3, 40);
	 set_motors(1, 30);
	 set_motors(5, 60);
	 hardware_exchange();
	 sleep1(1000);
	 set_motors(3, 48);
	 set_motors(1, 48);
	 set_motors(5, 48);
	 hardware_exchange();
	 for (int n = 0; n < 320; n++) {
		 error[n] = n - 160;
		 //cout<<error[n]<<endl;
	 }
	 return 0;
};


void Robot::SetMotors(int vLeft, int vRight){
	
	set_motors(1,vLeft);
	set_motors(5,vRight);
	hardware_exchange();
	
	
};
	
	
	





int Robot::MeasureLine(){
		
		take_picture();
		update_screen();
		int pix =0;
		int row = 120;
		int a[320]; // create array to pass initial values of pixels
		int line = 0;
		int nBlPixs = 0;
		
		for (int col = 0; col < 320 ; col++){
			pix = get_pixel(row,col,3);  
			if(pix<80){
				a[col]=1; // if white pixel value is below a certain threshold (of 'whiteness'), assign it to black pixel
	            nBlPixs++; 
			}
			else{
				a[col]=0; //else, if above threshold, assign it to white pixel
			}
			
			//cout<<a[col];
		}	
		//cout<<endl;
		line_present = 1;
		if (nBlPixs == 0 ){
			// no black line
			cout<<" No BLACK PIXELS!!!!!!!"<<endl;
			line_present = 0;
			return -1;
	     }
        else{	
	 	   for (int n = 0; n <320; n++) {
			  line = line + error[n] * a[n]; 
		   }
		  line = line /nBlPixs;
		  cout<<line<<endl;
		  line_error=line;
	     }
	return 0;
}

int Robot::Quad4(){
	return 0;
}

int Robot::Quad3(){
	return 0;
}

int Robot::OpenGate() {
	//make sure to be connected to wifi
	
	char server_addr[15] = {'1','3','0','.','1','9','5','.','1','9','6'};//on the green sheet of paper by gate
	char openPlease[24] ={'P','l','e','a','s','e'};
	char password[24];// will be set when connected to server
	
	connect_to_server(server_addr,1024);//connects to server with IP address and port 1024
	
	send_to_server(openPlease); //sends connection plea to server
	
	receive_from_server(password);//sever will change 'password' to the password we need
	
	send_to_server(password); //send server the password we received
	
	return 0;
	
}


int Robot::FollowLine(){
	//cout << "go here 123" << endl;
	MeasureLine();
	if (line_present) {
		kp=0.2;
		dv = (int)(line_error*kp);
		//dv = 0;
		v_left = v_left_go + dv;
		v_right = v_right_go + dv;
		cout<<" line_error= "<<line_error<<"\tdv = "<<dv<<" v_l="<<v_left<<" v_r="<<v_right<<endl;
		SetMotors(v_left,v_right); 
	} else {
		//go back
		cout<<" Line Missing"<<endl;
		v_left = 39;
		v_right = 55;
		if ( v_left<30) {v_left=30;}
		if ( v_right<30) {v_right=30;}
		if ( v_left>65) {v_left=65;}
		if ( v_right>65) {v_right=65;}
		SetMotors(v_left,v_right);
		sleep1(1000);
	}
	return 0;
};

int main() {
	Robot robot;
	robot.initHardware();
	open_screen_stream();
	int count = 0;
	while (count < 5000) {
		robot.FollowLine();
		
	}
	
	//while (quadrant<=4){
		//if (quadrant==1) {
			//robot.OpenGate(){}
			//quadrant++;
		//}
		//else if(quadrant==2) {
			//robot.MeasureLine();
			//robot.FollowLine();
		//}
		//else if(quadrant==3) {
			//robot.Quad3();
			//robot.FollowLine();
		//}
		//else if (quadrant==4) {
			//robot.Quad4();
		//}
		
	//}
	close_screen_stream();
	stoph();
	return 0;
	
}
