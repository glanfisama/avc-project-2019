#include <iostream>
#include "E101.h"

using namespace std;

class Robot { //decalarations
	private: //declearing private fields (only this class can change)
		int v_left, v_right, cam_tilt;
		int dv;
		double line_error;
		int quadrant;
		const int cam_width = 320;
		const int cam_height = 240;
		const int v_left_go = 52; //This will need to be change
		const int v_right_go = 43; //This will need to be change
		double kp = 0.05;
		bool line_present = true;
		int error[321];
		


	public: //declaring public fields (that other Classes can change)
		Robot(){} //default constructor (labeled same as class)
		//declaring methods
		int initHardware();
		void SetMotors(double vLeft, double vRight);
		int MeasureLine();
		int FollowLine();
};
//to create new methods for the robot make sure to put them under the publically
//decleared fields. 
//method set up:
// Class::method(){} //with return value


int Robot::initHardware() {
		
		int err;
  cout<<" Hello"<<endl;
  err = init(0);
  cout<<"After init() error="<<err<<endl;
  
  //int count = 0;
  open_screen_stream();
  take_picture();
  update_screen();
  
	 set_motors(3, 48);
	 set_motors(1, 48);
	 set_motors(5, 48);
	 hardware_exchange();
	 sleep1(1000);
	 set_motors(3, 40);
	 set_motors(1, 30);
	 set_motors(5, 60);
	 hardware_exchange();
	 sleep1(1000);
	 set_motors(3, 48);
	 set_motors(1, 48);
	 set_motors(5, 48);
	 hardware_exchange();
	
	for(int n = 0; n < 321; n++){
		error[n] = n-160;
		
	}


	 return 0;
};

void Robot::SetMotors(double vLeft, double vRight){
	set_motors(1,vLeft);
	set_motors(5,vRight);
	hardware_exchange();
};




int Robot::MeasureLine(){
	
	int count = 0;
	open_screen_stream();
	
	while(count < 500){
		take_picture();
		update_screen();
		int pix =0;
		int row = 120;
		int a[321]; // create array to pass initial values of pixels
		
	 
		for (int col = 0; col < 320 ; col++){
			pix = get_pixel(row,col,3);
			cout<<pix<<" ";  
	
			if(pix<80){
				a[col]=1; // if white pixel value is below a certain threshold (of 'whiteness'), assign it to black pixel
	
			}
			else{
				a[col]=0; //else, if above threshold, assign it to white pixel
			}
		}	
		
		
		for(int n = 0; n < 321; n++){
			line_error += a[n] * error[n];
		}
		

		cout<<endl;
		sleep1(300);
		
		count++;

	}
	
	return 0;
}




int Robot::FollowLine(){
	MeasureLine();
	if (line_present) {
		dv = (int)(line_error*kp);
		//dv = 0;
		v_left = v_left_go + dv;
		v_right = v_right_go +dv;
		cout<<" line_error = "<<line_error<<"\tdv = "<<dv<<endl;
		SetMotors(v_left, v_right); 
	} else {
		//go back
		cout<<" Line Missing"<<endl;
		v_left = 39;
		v_right = 55;
		SetMotors(v_left, v_right); 
		sleep1(1000);
	}
	return 0;
};

int main() {
	Robot robot;
	robot.initHardware();
	//int count = 0;
	//open_screen_stream();
	
	//while (count < 5000){
		//robot.MeasureLine();
		//robot.FollowLine();
		//count++;
	//}
	//close_screen_stream();
	//stoph();
	return 0;
	
}

